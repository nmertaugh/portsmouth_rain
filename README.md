# Portsmouth Rain

It was raining that day... We had oysters and Prosecco...

```
mogrify -resize 1900 *.jpg
```

```
current_num_photos = 69
directory = '/home/nmertaugh/tmp/portsmouth_rain_2019'
photo_files = Dir.entries(directory).select { |x| x.match(/.*\.JPG$|.*\.jpg$/) }

photo_files.each_with_index do |file, index|
  File.rename "#{directory}/#{file}", "#{directory}/tmp_background_#{index + current_num_photos + 1}.jpg"
end

photo_files = Dir.entries(directory).select { |x| x.match(/.*\.jpg$/) }

photo_files.each_with_index do |file, index|
  final_file_name = file.gsub('tmp_', '')
  File.rename "#{directory}/#{file}", "#{directory}/#{final_file_name}"
end
```
