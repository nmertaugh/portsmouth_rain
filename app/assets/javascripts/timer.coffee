class Timer
  constructor: ->
    @firstDateTime = new Date('2016-10-09T16:00:00.000Z')
    @totalNumPhotos = 70
    @currentPhotoIndexBottom = 3
    @currentPhotoIndexTop = 1
    @photoIndices = this._shuffle([1..@totalNumPhotos])
    @assetPath = 'https://f002.backblazeb2.com/file/nm-portsmouth-rain-2019'

  initializeCountDisplays: ->
    timer = this

    setInterval ->
      timer._updateDisplay()
    , 1

    setTimeout ->
      timer._updateBackground()
    , 6000

  _currentTime: ->
    new Date

  _daysSince: ->
    this._hoursSince() / 24

  _hoursSince: ->
    this._minutesSince() / 60

  _labelForDays: ->
    parseInt(
      this._daysSince() -
      (this._labelForYears() * 365)
    )

  _labelForHours: ->
    parseInt(
      this._hoursSince() -
      (this._labelForYears() * 365 * 24) -
      (this._labelForDays() * 24)
    )

  _labelForMilliseconds: ->
    parseInt(
      this._millisecondsSince() -
      (this._labelForYears() * 365 * 24 * 60 * 60 * 1000) -
      (this._labelForDays() * 24 * 60 * 60 * 1000) -
      (this._labelForHours() * 60 * 60 * 1000) -
      (this._labelForMinutes() * 60 * 1000) -
      (this._labelForSeconds() * 1000)
    )

  _labelForMinutes: ->
    parseInt(
      this._minutesSince() -
      (this._labelForYears() * 365 * 24 * 60) -
      (this._labelForDays() * 24 * 60) -
      (this._labelForHours() * 60)
    )

  _labelForSeconds: ->
    parseInt(
      this._secondsSince() -
      (this._labelForYears() * 365 * 24 * 60 * 60) -
      (this._labelForDays() * 24 * 60 * 60) -
      (this._labelForHours() * 60 * 60) -
      (this._labelForMinutes() * 60)
    )

  _labelForYears: ->
    parseInt(this._yearsSince())

  _millisecondsSince: ->
    this._currentTime() - @firstDateTime

  _minutesSince: ->
    this._secondsSince() / 60

  _nextPhotoIndexBottom: ->
    if @currentPhotoIndexBottom >= @totalNumPhotos - 1
      @currentPhotoIndexBottom = 0
    else
      @currentPhotoIndexBottom += 1

  _nextPhotoIndexTop: ->
    if @currentPhotoIndexTop >= @totalNumPhotos - 1
      @currentPhotoIndexTop = 0
    else
      @currentPhotoIndexTop += 1

  _secondsSince: ->
    this._millisecondsSince() / 1000

  _shuffle: (source) ->
    return source unless source.length >= 2

    for index in [source.length-1..1]
      randomIndex = Math.floor Math.random() * (index + 1)
      [source[index], source[randomIndex]] = [source[randomIndex], source[index]]

    source

  _updateBackground: ->
    timer = this
    bottomBackground = $('.bottom-background')
    topBackground = $('.top-background')
    bottomBackgroundClasses = bottomBackground.prop('class').split(/\s+/)
    topBackgroundClasses = topBackground.prop('class').split(/\s+/)

    if topBackground.css('opacity') == '1'
      currentlyVisibleBackground = topBackground
      currentlyVisibleBackgroundClasses = topBackgroundClasses
      currentlyVisiblePhotoNumber = @photoIndices[this._nextPhotoIndexTop()]
      nextToBeVisibleBackground = bottomBackground
      nextToBeVisibleBackgroundClasses = bottomBackgroundClasses
      nextToBeVisiblePhotoNumber = @photoIndices[this._nextPhotoIndexBottom()]
    else
      currentlyVisibleBackground = bottomBackground
      currentlyVisibleBackgroundClasses = bottomBackgroundClasses
      currentlyVisiblePhotoNumber = @photoIndices[this._nextPhotoIndexBottom()]
      nextToBeVisibleBackground = topBackground
      nextToBeVisibleBackgroundClasses = topBackgroundClasses
      nextToBeVisiblePhotoNumber = @photoIndices[this._nextPhotoIndexTop()]

    currentlyVisibleBackground.css 'z-index', 10

    nextToBeVisibleBackground.css 'z-index', 1
    nextToBeVisibleBackground.removeClass 'transitions-enabled'
    nextToBeVisibleBackground.css 'opacity', 1

    nextToBeVisibleBackground.find('img').prop('src', "#{@assetPath}/background_#{nextToBeVisiblePhotoNumber}.jpg");

    currentlyVisibleBackground.addClass 'transitions-enabled'
    assetPath = @assetPath;

    setTimeout ->
      currentlyVisibleBackground.css 'opacity', 0

      setTimeout ->
        currentlyVisibleBackground.find('img').prop('src', "#{assetPath}/background_#{nextToBeVisiblePhotoNumber}.jpg");

        setTimeout ->
          timer._updateBackground()
        , 2000
      , 2000
    , 3000

  _updateDisplay: ->
    $('#timer .days').html this._labelForDays() + 'd'
    $('#timer .hours').html this._labelForHours() + 'h'
    $('#timer .milliseconds').html this._labelForMilliseconds() + 'ms'
    $('#timer .minutes').html this._labelForMinutes() + 'm'
    $('#timer .seconds').html this._labelForSeconds() + 's'
    $('#timer .years').html this._labelForYears() + 'y'

  _yearsSince: ->
    this._daysSince() / 365

window.initializeTimer = ->
  timer = new Timer()
  timer.initializeCountDisplays()
